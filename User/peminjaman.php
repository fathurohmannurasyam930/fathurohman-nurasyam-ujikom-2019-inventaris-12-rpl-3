<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
<header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Peminjaman</h3>
                        </div>
                    </div> 
                </div>
            </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                        <div class="row" align="center">
                          <div class="col-lg-12">
                                          <div class="au-card recent-report">
                                            
                                            <img src="../images/01.jpg">
                                            <br>
                                            <br>
                                             <h2>Peminjaman Barang</h2>
                                             <span></span>
                                             <hr>
                                             <a href="pinjam.php?kode_db=<?php echo $kode ?>&kode_pinjam=<?php echo $kd ?>"><button class="btn btn-info" type="button">Peminjaman Barang</button></a>
                                             
                                             <br>
                                             <br>
                                          </div>
                          </div> 
                       </div>

                       <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                    <center><h4>Detail Peminjaman</h4>
                                     <br>
                                    </center>
                                   
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>Kode Peminjam</th>
                                                                <th>Kode Barang</th>
                                                                <th>Tanggal Pinjam</th>
                                                                <th>Status</th>
                                                                <th>Pegawai</th>
                                                                <th>Kelas</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($db->peminjam() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['kode_peminjaman'];?></td>
                                                                    <td><?php echo $x['kode_inventaris'];?></td>
                                                                    <td><?php echo $x['tanggal_pinjam']; ?></td>
                                                                    <td><?php echo $x['status_peminjaman']; ?></td>
                                                                    <td><?php echo $x['kode_pegawai']; ?></td>
                                                                    <td><?php echo $x['kelas']; ?></td>
                                                                    <td>
                                                                        <a href="liat_pinjam.php?kode_peminjaman=<?php echo $x['kode_peminjaman'];?>">
                                                                        <button type="reset" class="btn btn-success btn-sm">
                                                                            <i class="fas fa-eye"></i> Detail
                                                                        </button>
                                                                        </a>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                     
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                        
                       
            </div>

           

	            <?php
	            include"footer.php";
	            ?>
	            
        </div>
    </div>

    
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>
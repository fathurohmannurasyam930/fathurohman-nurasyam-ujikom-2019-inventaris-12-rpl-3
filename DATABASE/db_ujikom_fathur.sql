-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 06:03 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ujikom_fathur`
--

-- --------------------------------------------------------

--
-- Table structure for table `asal_barang`
--

CREATE TABLE `asal_barang` (
  `id` int(15) NOT NULL,
  `asal_barang` varchar(255) NOT NULL,
  `kode_asalbarang` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asal_barang`
--

INSERT INTO `asal_barang` (`id`, `asal_barang`, `kode_asalbarang`) VALUES
(1, 'Animasi', 'KAB01'),
(2, 'Rekayasa Perangkat lunak', 'KAB02'),
(3, 'Brocasting', 'KAB03'),
(4, 'Teknik Kendaran Ringan', 'KAB04'),
(5, 'Teknik Pengelasan', 'KAB05'),
(6, 'TU', 'KAB06'),
(7, 'Musolah', 'KAB07'),
(8, 'Guru', 'KAB08'),
(9, 'Perpustakaan', 'KAB09');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL,
  `kode_detail_pinjam` varchar(200) NOT NULL,
  `kode_peminjaman` varchar(200) NOT NULL,
  `kode_inventaris` varchar(200) NOT NULL,
  `jumlah` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id`, `kode_detail_pinjam`, `kode_peminjaman`, `kode_inventaris`, `jumlah`) VALUES
(2, 'KDP01', 'KP01', 'KI01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `kode_inventaris` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `asal_barang` varchar(200) NOT NULL,
  `kondisi` varchar(200) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `petugas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`kode_inventaris`, `nama_barang`, `asal_barang`, `kondisi`, `jumlah`, `jenis`, `ruang`, `tanggal_register`, `keterangan`, `petugas`) VALUES
('KI01', 'Laptop', 'Rekayasa Perangkat lunak', 'Baik', 99, 'Lenovo', 'Lab RPL 1', '2019-03-29 00:00:00', '-', 'Asyam'),
('KI02', 'Infokus', 'Rekayasa Perangkat lunak', 'Baik', 99, 'Infokus', 'TU', '2019-03-29 00:00:00', '-', 'Asyam'),
('KI03', 'Laptop', 'Animasi', 'Baik', 98, 'Hp', 'Studio Animasi', '2019-04-01 00:00:00', '-', 'Asyam');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL,
  `nama_jenis` varchar(200) NOT NULL,
  `kode_jenis` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Lenovo', '0KJ1', '-'),
(2, 'Hp', '0KJ2', '-'),
(3, 'Infokus', '0KJ3', '-');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `kode_level` varchar(200) NOT NULL,
  `nama_level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`kode_level`, `nama_level`) VALUES
('KL01', 'Admin'),
('KL02', 'Operator'),
('KL03', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `kode_pegawai` varchar(11) NOT NULL,
  `nip` char(15) NOT NULL,
  `nama_pegawai` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`kode_pegawai`, `nip`, `nama_pegawai`, `username`, `password`) VALUES
('KP01', '090909', 'AW', 'AW', '123'),
('KP02', '808080', 'ANDI', 'ANDI', '543');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `kode_peminjaman` varchar(15) NOT NULL,
  `kode_detail_pinjam` varchar(200) NOT NULL,
  `kode_inventaris` varchar(255) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(200) NOT NULL,
  `kode_pegawai` varchar(200) NOT NULL,
  `kelas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`kode_peminjaman`, `kode_detail_pinjam`, `kode_inventaris`, `jumlah`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `kode_pegawai`, `kelas`) VALUES
('KP29', 'KDP81', 'KI01', 4, '2019-04-02 00:00:00', '0000-00-00 00:00:00', 'Barang Dipinjam Belum DiKembalikan', 'ANDI', '12 RPL 1'),
('KP42', 'KDP64', 'KI03', 2, '2019-04-02 00:00:00', '0000-00-00 00:00:00', 'Barang Dipinjam Belum DiKembalikan', 'AW', '10 ANM 2'),
('KP51', 'KDP01', 'KI02', 1, '2019-04-01 00:00:00', '2019-04-01 00:00:00', 'Telah DiKembalikan', 'Handsome', '11 RPL 2');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id` int(11) NOT NULL,
  `kode_petugas` varchar(15) NOT NULL,
  `nama_petugas` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id`, `kode_petugas`, `nama_petugas`, `username`, `password`, `level`) VALUES
(1, 'KPTG01', 'Fathrohman Nurasyam', 'FN99', 'admin', 'Admin'),
(2, 'KPTG02', 'Raja', 'Raja02', 'op', 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL,
  `nama_ruang` varchar(200) NOT NULL,
  `kode_ruang` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab Animasi', 'KR01', '-'),
(2, 'Lab RPL 1', 'KR02', '-'),
(3, 'Studio Animasi', 'KR03', '-'),
(4, 'TU', 'KR04', '-'),
(5, 'Lab RPL 2', 'KR05', '-'),
(6, 'Musolah', 'KR06', '-'),
(7, 'Perpustakaan', 'KR07', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asal_barang`
--
ALTER TABLE `asal_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD UNIQUE KEY `kode_inventaris` (`kode_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`),
  ADD UNIQUE KEY `id_jenis` (`id_jenis`),
  ADD UNIQUE KEY `kode_jenis` (`kode_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`kode_level`),
  ADD UNIQUE KEY `id_level` (`kode_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`kode_pegawai`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`kode_peminjaman`),
  ADD UNIQUE KEY `id_peminjaman` (`kode_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_petugas` (`kode_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`),
  ADD UNIQUE KEY `id_ruang` (`id_ruang`),
  ADD UNIQUE KEY `kode_ruang` (`kode_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asal_barang`
--
ALTER TABLE `asal_barang`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

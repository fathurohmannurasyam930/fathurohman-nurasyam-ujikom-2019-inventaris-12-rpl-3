<?php
include"header.php";
include 'database/class.php';
    $kode_db=$_GET['kode_db'];
     $kode_pinjam=$_GET['kode_pinjam'];
$db = new database();
?>

				<header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Form Pinjam</h3>

                        </div>
                    </div>
                </div>
                </header>

                <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Peminjam
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah_pinjam" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Detail Peminjam</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                        <input type="text" class="form-control" name="kode_detail_pinjam" readonly value="<?php echo $kode_db ?>">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Peminjaman</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                        <input type="text" class="form-control" name="kode_peminjaman" readonly value="<?php echo $kode_pinjam ?>">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                       <select name="kode_pegawai" id="select" class="form-control" required>
                                                         <?php  
                                                        foreach ($db->pegawai() as $pegawai) {
                                                        ?>
                                                <option value="<?php echo $pegawai['nama_pegawai']; ?>" ><?php echo $pegawai['nama_pegawai']; ?>
                                                        </option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                       <label for="select" class=" form-control-label">Kelas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                          <select name="kelas" id="select" type="text" class="form-control">
                                                            <option>Kelas</option>
                                                            <option>10 ANM 1</option>
                                                            <option>10 ANM 2</option>
                                                            <option>11 ANM 1</option>
                                                            <option>11 ANM 2</option>
                                                            <option>12 ANM 1</option>
                                                            <option>12 ANM 2</option>
                                                            <option>10 BC</option>
                                                            <option>11 BC</option>
                                                            <option>12 BC</option>
                                                            <option>10 RPL 1</option>
                                                            <option>10 RPL 2</option>
                                                            <option>10 RPL 3</option>
                                                            <option>11 RPL 1</option>
                                                            <option>11 RPL 2</option>
                                                            <option>11 RPL 3</option>
                                                            <option>12 RPL 1</option>
                                                            <option>12 RPL 2</option>
                                                            <option>12 RPL 3</option>
                                                            <option>10 TKR 1</option>
                                                            <option>10 TKR 2</option>
                                                            <option>11 TKR 1</option>
                                                            <option>11 TKR 2</option>
                                                            <option>12 TKR 1</option>
                                                            <option>12 TKR 2</option>
                                                            <option>10 TPL</option>
                                                            <option>11 TPL</option>
                                                            <option>12 TPL</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Barang</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                         <input type="text" name="kode_inventaris" class="form-control" placeholder="Kode Barang">
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Jumlah</label>
                                                    </div>
                                                    <div class="col-12 col-md-2">
                                                         <input type="number" name="jumlah" class="form-control" placeholder="Jumlah">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                                
                                            </div>
                                        </form>
                                    </div>
                    </div> 
                </div>


	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>
<?php  
  $koneksi= new mysqli("localhost","root","","db_ujikom_fathur");
$output = '';
if(isset($_POST["export"]))
{
 $result = $koneksi->query("SELECT * FROM peminjaman");
 if($result->num_rows > 0)
 {
  $output .= '
   <table class="table" border="1">  
                    <tr>
                    <th>Kode Peminjaman</th>
                    <th>Kode Detail Pinjama</th>
                    <th>Kode Inventaris</th>
                    <th>Jumlah</th>
                    <th>Tanggal Peminjaman</th>
                    <th>Tanggal Pengembalian</th>
                    <th>Status</th>
                    <th>Pegawai</th>
                    <th>Kelas</th>
                    </tr>
  ';
  while($row = $result->fetch_array())
  {
   $output .= '
  <tr>  
     <td>'.$row["kode_peminjaman"].'</td>  
     <td>'.$row["kode_detail_pinjam"].'</td>  
     <td>'.$row["kode_inventaris"].'</td>  
     <td>'.$row["jumlah"].'</td>
     <td>'.$row["tanggal_pinjam"].'</td>
     <td>'.$row["tanggal_kembali"].'</td>
     <td>'.$row["status_peminjaman"].'</td>
     <td>'.$row["kode_pegawai"].'</td>
     <td>'.$row["kelas"].'</td>
    </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Laporan Peminjaman Dan Pengembalian.xls');
  echo $output;
 }
}
?>
<?php 

	$koneksi= new mysqli("localhost","root","","db_ujikom_fathur");

 ?>

 <style>
 	@media print{
 		input.noPrint{
 			display: none;
 		}
 	}
 </style>

<table border="1" width="100%" style="border-collapse: collapse;">
	<caption><h1>Laporan Peminjaman dan Pengembalian</h1></caption>
	<thead>
	<tr>
		<th>NO</th>
		<th>Kode Peminjaman</th>
		<th>Kode Detail Pinjama</th>
		<th>Kode Inventaris</th>
		<th>Jumlah</th>
		<th>Tanggal Peminjaman</th>
		<th>Tanggal Pengembalian</th>
		<th>Status</th>
		<th>Pegawai</th>
		<th>Kelas</th>

	</tr>
	</thead>
	<tbody>
<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
       $no=1;
		$sql=$koneksi->query("SELECT * FROM peminjaman ");
		while($data=$sql->fetch_array()){
	 ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['kode_peminjaman']; ?></td>
        <td><?php echo $data['kode_detail_pinjam']; ?></td>
        <td><?php echo $data['kode_inventaris']; ?></td>
        <td><?php echo $data['jumlah']; ?></td>
        <td><?php echo $data['tanggal_pinjam']; ?></td>
        <td><?php echo $data['tanggal_kembali']; ?></td>
        <td><?php echo $data['status_peminjaman']; ?></td>
        <td><?php echo $data['kode_pegawai']; ?></td>
        <td><?php echo $data['kelas']; ?></td>


    </tr>
<?php  } ?>
	</tbody>
</table>

<br>
 
 <form method="POST" action="laporan_pinjam_exsel.php">
  <input type="button" value="Cetak" class="noPrint" onclick="window.print()">
  <input type="submit" name="export" class="btn btn-success noPrint" value="Export" />
</form> 

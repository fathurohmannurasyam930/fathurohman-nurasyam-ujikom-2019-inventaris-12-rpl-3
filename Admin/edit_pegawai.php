<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Jenis</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Edit Ruang
                                        </div>
                                        <div class="card-body card-block">
                                            <?php
                                                    include"database/koneksi.php";
                                                    $kode_pegawai=$_GET['kode_pegawai'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM pegawai WHERE kode_pegawai='$kode_pegawai'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                            <form action="" method="post" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class="form-control-label">Kode Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="hidden" name="kode_pegawai" value="<?php echo $_GET['kode_pegawai'];?>">
                                                    <input type="text" name="kode_pegawai" class="form-control" value="<?php echo $tampil['kode_pegawai'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">NIP</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nip" class="form-control" value="<?php echo $tampil['nip'];?>" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_pegawai" class="form-control" value="<?php echo $tampil['nama_pegawai'];?>" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Username</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="username" class="form-control" value="<?php echo $tampil['username'];?>" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="password" class="form-control" value="<?php echo $tampil['password'];?>" required>
                                                    </div>
                                                </div>


                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" name="edit" value="edit">
                                                </div>
                                            </form>
                                           <?php
                                            include"database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $kode_pegawai=$_POST['kode_pegawai'];
                                                $nip=$_POST['nip'];
                                                $nama_pegawai=$_POST['nama_pegawai'];
                                                $username=$_POST['username'];
                                                $password=$_POST['password'];

                                                $input=mysqli_query($koneksi, "UPDATE pegawai SET kode_pegawai='$kode_pegawai', nip='$nip', nama_pegawai='$nama_pegawai', username='$username', password='$password'  WHERE kode_pegawai='$kode_pegawai'");

                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="pengguna.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
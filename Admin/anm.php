<?php
include"header.php";
?>

				<header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Animasi</h3>

                        </div>
                    </div>
                </div>
                </header>

                <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                     <div class="row">
                                            <div class="col-md-6">
                                                 Barang ANM
                                            </div>

                                            <div class="col-md-6" align="right">
                                         <a href="import/laporan_anm.php">
                                        <button class="btn btn-danger btn-sm">
                                            <i class=""></i>CETAK & EXPORT</button>
                                        </a>
                                            </div>

                                       
                                         
                                        </div>
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>Kode Barang</th>
                                                                <th>Nama Barang</th>
                                                                <th>Kondisi</th>
                                                                <th>Tanggal Register</th>
                                                                <th>Ruang</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                       <tbody>
                                                        <?php
                                                         include"database/koneksi.php";
                                                         $no=1;
                                                            $pilih=mysqli_query($koneksi, "SELECT * FROM inventaris where asal_barang='ANIMASI'");
                                                            while($data=mysqli_fetch_array($pilih)){
                                                        ?> 
                                                        <tr>
                                                        <td><?=$data['kode_inventaris']; ?></td>
                                                        <td><?=$data['nama_barang']; ?>
                                                            /<?=$data['jenis']; ?>
                                                        </td>
                                                        <td><?=$data['kondisi']; ?></td>
                                                        <td><?=$data['tanggal_register'];?></td>
                                                        <td><?=$data['ruang'];?></td>
                                                        <td>
                                                        <a href="edit_barang.php?kode_inventaris=<?php echo $x['kode_inventaris'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fas fa-edit"></i> Edit
                                                                        </button>
                                                                        </a>
                                                        <a href="hps_inven.php?kode_inventaris=<?php echo $data['kode_inventaris'];?>">
                                                            <button type="reset" class="btn btn-danger btn-sm">
                                                            <i class="fa fa-ban"></i> Hapus
                                                            </button>
                                                        </a>
                                                         </td>
                                                        </tr>
                                                       </tbody>
                                                       <?php 
                                                        $no++;    
                                                        } ?> 
                                                    </table>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>


	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>

<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>
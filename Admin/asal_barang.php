<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Data Asal Barang </h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                     <h4>Data Asal Barang</h2>
                                     <br>
                                     <a href="tambah_asalbarang.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>Tambah Data Asal Barang</button>
                                    </a>
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>NO</th>
                                                                <th>Kode Asal barang</th>
                                                                <th>Asal Barang</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($db->asal_barang () as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['id'];?></td>
                                                                    <td><?php echo $x['kode_asalbarang'];?></td>
                                                                    <td><?php echo $x['asal_barang']; ?></td>
                                                                    <td>
                                                                        <a href="edit_asalbarang.php?id=<?php echo $x['id'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                        <a href="pro_inven.php?kode_asalbarang=<?php echo $x['kode_asalbarang']; ?>&aksi=hapus_ab">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>
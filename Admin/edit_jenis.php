<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Jenis</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Edit Ruang
                                        </div>
                                        <div class="card-body card-block">
                                            <?php
                                                    include"database/koneksi.php";
                                                    $id_jenis=$_GET['id_jenis'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM jenis WHERE id_jenis='$id_jenis'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                            <form action="" method="post" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class="form-control-label">Kode Jenis</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="hidden" name="id_jenis" value="<?php echo $_GET['id_jenis'];?>">
                                                    <input type="text" name="kode_jenis" class="form-control" value="<?php echo $tampil['kode_jenis'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Jenis</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_jenis" class="form-control" value="<?php echo $tampil['nama_jenis'];?>" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="keterangan" class="form-control" value="<?php echo $tampil['keterangan'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" name="edit" value="edit">
                                                </div>
                                            </form>
                                           <?php
                                            include"database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $id_jenis=$_POST['id_jenis'];
                                                $kode_jenis=$_POST['kode_jenis'];
                                                $nama_jenis=$_POST['nama_jenis'];
                                                $keterangan=$_POST['keterangan'];

                                                $input=mysqli_query($koneksi, "UPDATE jenis SET kode_jenis='$kode_jenis', nama_jenis='$nama_jenis', keterangan='$keterangan' WHERE id_jenis='$id_jenis'");

                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="jenis.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Data Ruangan </h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                     <h4>Data Ruangan</h2>
                                     <br>
                                     <a href="tambah_ruang.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>Tambah Data Ruang</button>
                                    </a>
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>NO</th>
                                                                <th>Kode Ruang</th>
                                                                <th>Nama Ruang</th>
                                                                <th>Keteranagan</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($db->ruang () as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['id_ruang']; ?></td>
                                                                    <td><?php echo $x['kode_ruang'];?></td>
                                                                    <td><?php echo $x['nama_ruang']; ?></td>
                                                                    <td><?php echo $x['keterangan']; ?></td>
                                                                    <td>
                                                                        <a href="edit_ruang.php?id_ruang=<?php
                                    echo $x['id_ruang'];?>">
                                                                         <button type="submit" class="btn btn-primary btn-sm">
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        </button>
                                                                        </a>
                                                                        <a href="pro_inven.php?kode_ruang=<?php echo $x['kode_ruang']; ?>&aksi=hapus_ruang">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>           
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>

                </div>
            </div>
            <hr>

	            <?php
	            include"footer.php";
	            ?>
	            
</div>
</div>
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>